//
//  ViewController.h
//  nsuserdefault
//
//  Created by Prince Prabhakar on 17/11/15.
//  Copyright (c) 2015 Prince Prabhakar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *userName;
@property (strong, nonatomic) IBOutlet UITextField *passWord;
- (IBAction)submit:(id)sender;


@end

