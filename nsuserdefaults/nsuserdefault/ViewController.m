//
//  ViewController.m
//  nsuserdefault
//
//  Created by Prince Prabhakar on 17/11/15.
//  Copyright (c) 2015 Prince Prabhakar. All rights reserved.
//

#import "ViewController.h"
NSString *userName;
NSString *passWord;
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   NSString *name=[[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
     NSString *pass=[[NSUserDefaults standardUserDefaults]objectForKey:@"pass"];
    
    NSLog(@"%@",name);
    NSLog(@"%@",pass);
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submit:(id)sender
{
    userName=_userName.text;
    passWord=_passWord.text;
    
    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:@"username"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:passWord forKey:@"password"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
@end
